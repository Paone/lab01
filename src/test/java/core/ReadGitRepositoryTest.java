package core;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;


public class ReadGitRepositoryTest {

	private  String repoPath;
	private  GitRepository repository;
	
	@Before
	public void setup() {
		repoPath = new String("sample_repos/sample01");
		repository = new GitRepository(repoPath);
	}
	
	@Test
	public void shouldFindHead() throws Exception {
		assertThat(repository.getHeadRef()).isEqualTo("refs/heads/master");
	}
	
	//TODO: trovare da shell l'hash del commit relativo al master del repository in sample_repos/sample01
	private static String masterCommitHash = "???"; 

	@Test
	public void shouldFindHash() throws Exception {
		assertThat(repository.getRefHash("refs/heads/master")).isEqualTo(masterCommitHash);
	}

}
