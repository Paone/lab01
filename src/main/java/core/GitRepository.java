package core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class GitRepository {

	private String repoPath;

	public GitRepository(String repoPath) {
		this.repoPath = repoPath;
	}

	public String getHeadRef() throws IOException {
		try(BufferedReader bf = new BufferedReader(new FileReader(repoPath+"/HEAD"))){
			return bf.readLine().substring(5);
		}
	}

}